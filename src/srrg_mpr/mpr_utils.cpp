#include "mpr_utils.h"

namespace srrg_mpr {
using namespace srrg_core;

void shrinkDepth(
    FloatImage& dest, const FloatImage& src, int kr, int kc, bool interpolate) {
  int rows = src.rows;
  int cols = src.cols;

  if (rows % kr) {
    std::cerr << "shrinkRawDepth(...) : rows=" << rows << ",  kr:" << kr
              << std::endl;
    throw std::runtime_error(
        "shrinkDepth: fatal, the shrink factor should be perfect divider of "
        "the image rows");
  }

  if (cols % kc) {
    std::cerr << "shrinkRawDepth(...) : cols=" << cols << " kc:" << kc
              << std::endl;
    throw std::runtime_error(
        "shrinkDepth: fatal, the shrink factor should be perfect divider of "
        "the image cols");
  }

  int drows = rows / kr;
  int dcols = cols / kc;

  dest.create(drows, dcols);
  dest = 0;

  // how many active pixels in a bin

  IntImage counters;

  if (interpolate) {
    counters.create(drows, dcols);
    counters = 0;
  }

  // avoid divisions and use a lookup table
  int rtable[rows];
  for (int r = 0; r < rows; ++r) rtable[r] = r / kr;

  int ctable[cols];
  for (int c = 0; c < cols; ++c) ctable[c] = c / kc;

  for (int r = 0; r < rows; ++r) {
    // get the row pointers of source and destination

    const float* src_ptr = src.ptr<const float>(r);
    int dr = rtable[r];
    float* dest_ptr = dest.ptr<float>(dr);
    int* counters_ptr = interpolate ? counters.ptr<int>(dr) : 0;

    int cc = 0;
    for (int c = 0; c < cols; ++c, ++src_ptr) {
      const float src_z = *src_ptr;
      if (src_z == 0) continue;
      float& dest_z = *(dest_ptr + ctable[c]);
      if (interpolate) {
        dest_z += src_z;
        int& counter = counters_ptr[ctable[c]];
        counter++;
      } else {
        if (!dest_z || dest_z < src_z) dest_z = src_z;
      }
    }
  }

  if (interpolate) {
    for (int r = 0; r < drows; ++r) {
      float* dest_ptr = dest.ptr<float>(r);
      const int* counter_ptr = counters.ptr<const int>(r);
      for (int c = 0; c < dcols; ++c, ++dest_ptr, ++counter_ptr) {
        if (*counter_ptr) (*dest_ptr) *= 1. / (*counter_ptr);
      }
    }
  }
}

void shrinkIntensity(FloatImage& dest,
                     const FloatImage& src,
                     const FloatImage& src_depth,
                     int kr,
                     int kc) {
  int rows = src.rows;
  int cols = src.cols;

  if (rows % kr) {
    std::cerr << "shrinkRawDepth(...) : rows=" << rows << ",  kr:" << kr
              << std::endl;
    throw std::runtime_error(
        "shrinkDepth: fatal, the shrink factor should be perfect divider of "
        "the image rows");
  }

  if (cols % kc) {
    std::cerr << "shrinkRawDepth(...) : cols=" << cols << " kc:" << kc
              << std::endl;
    throw std::runtime_error(
        "shrinkDepth: fatal, the shrink factor should be perfect divider of "
        "the image cols");
  }

  int drows = rows / kr;
  int dcols = cols / kc;

  dest.create(drows, dcols);
  dest = 0;

  // how many active pixels in a bin

  IntImage counters;

  counters.create(drows, dcols);
  counters = 0;

  // avoid divisions and use a lookup table
  int rtable[rows];
  for (int r = 0; r < rows; ++r) rtable[r] = r / kr;

  int ctable[cols];
  for (int c = 0; c < cols; ++c) ctable[c] = c / kc;

  for (int r = 0; r < rows; ++r) {
    // get the row pointers of source and destination

    const float* src_ptr = src.ptr<const float>(r);
    const float* src_depth_ptr = src_depth.ptr<const float>(r);
    int dr = rtable[r];
    float* dest_ptr = dest.ptr<float>(dr);
    int* counters_ptr = counters.ptr<int>(dr);

    int cc = 0;
    for (int c = 0; c < cols; ++c, ++src_ptr, ++src_depth_ptr) {
      const float src_intensity = *src_ptr;
      const float src_depth_z = *src_depth_ptr;
      if (src_depth_z == 0) continue;
      float& dest_intensity = *(dest_ptr + ctable[c]);
      dest_intensity += src_intensity;
      int& counter = counters_ptr[ctable[c]];
      counter++;
    }
  }

  for (int r = 0; r < drows; ++r) {
    float* dest_ptr = dest.ptr<float>(r);
    const int* counter_ptr = counters.ptr<const int>(r);
    for (int c = 0; c < dcols; ++c, ++dest_ptr, ++counter_ptr) {
      if (*counter_ptr) (*dest_ptr) *= 1. / (*counter_ptr);
    }
  }
}

Float3Image replicateChannels(const FloatImage& src) {
  int rows = src.rows;
  int cols = src.cols;
  Float3Image dest;
  dest.create(rows, cols);
  for (int r = 0; r < rows; ++r) {
    cv::Vec3f* dest_ptr = dest.ptr<cv::Vec3f>(r);
    const float* src_ptr = src.ptr<const float>(r);
    for (int c = 0; c < cols; ++c, ++dest_ptr, ++src_ptr) {
      cv::Vec3f& v = *dest_ptr;
      v[0] = v[1] = v[2] = *src_ptr;
    }
  }
  return dest;
}

void tileImages(cv::Mat& dest,
                const std::vector<cv::Mat> src_images,
                bool horizontal) {
  if (horizontal) {
    int dest_cols = 0;
    int dest_rows = 0;
    int dest_type = -1;
    for (const cv::Mat& src : src_images) {
      if (dest_rows > 0 && src.rows != dest_rows)
        throw std::runtime_error("error in horizontal tile, rows should match");
      if (dest_type >= 0 && src.type() != dest_type)
        throw std::runtime_error("error in horizontal tile, rows should match");
      dest_rows = src.rows;
      dest_cols += src.cols;
      dest_type = src.type();
    }
    dest.create(dest_rows, dest_cols, dest_type);

    int current_col = 0;
    for (const cv::Mat& src : src_images) {
      cv::Mat dest_rect = dest(cv::Rect(current_col, 0, src.cols, src.rows));
      src.copyTo(dest_rect);
      current_col += src.cols;
    }
  } else {
    int dest_cols = 0;
    int dest_rows = 0;
    int dest_type = -1;
    for (const cv::Mat& src : src_images) {
      if (dest_cols > 0 && src.cols != dest_cols)
        throw std::runtime_error("error in horizontal tile, rows should match");
      if (dest_type >= 0 && src.type() != dest_type)
        throw std::runtime_error("error in horizontal tile, rows should match");
      dest_rows += src.rows;
      dest_cols = src.cols;
      dest_type = src.type();
    }
    dest.create(dest_rows, dest_cols, dest_type);

    int current_row = 0;
    for (const cv::Mat& src : src_images) {
      cv::Mat dest_rect = dest(cv::Rect(0, current_row, src.cols, src.rows));
      src.copyTo(dest_rect);
      current_row += src.rows;
    }
  }
}

// bdc, for evaluation in TUM format only
Vector7f t2vFull(const Eigen::Isometry3f& iso) {
  Vector7f v;
  v.head<3>() = iso.translation();
  Eigen::Quaternionf q(iso.linear());
  v(3) = q.x();
  v(4) = q.y();
  v(5) = q.z();
  v(6) = q.w();
  return v;
}

//! utility to convert the spherical camera matrix (Vector4f) to a
//! standard, pinhole-like, camera matrix (Matrix3f)
void convertCameraMatrix(Eigen::Matrix3f& K,
                         const Eigen::Vector4f& spherical_k,
                         const int rows,
                         const int cols) {
  K << spherical_k(2), 0, cols / 2, 0, spherical_k(3), rows / 2, 0, 0, 1;
}
}
