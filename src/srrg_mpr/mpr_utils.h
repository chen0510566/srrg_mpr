#pragma once
#include "srrg_types/cloud_3d.h"
#include "srrg_types/defs.h"

namespace srrg_mpr {
using namespace srrg_core;

Float3Image replicateChannels(const FloatImage& src);

void tileImages(cv::Mat& dest,
                const std::vector<cv::Mat> src_images,
                bool horizontal);

void shrinkDepth(FloatImage& dest,
                 const FloatImage& src,
                 int kr,
                 int kc,
                 bool interpolate = true);

void shrinkIntensity(FloatImage& dest,
                     const FloatImage& src,
                     const FloatImage& src_depth,
                     int kr,
                     int kc);

void convertCameraMatrix(Eigen::Matrix3f& dest,
                         const Eigen::Vector4f& src,
                         const int rows,
                         const int cols);

// bdc, for evaluation in TUM format only
typedef Eigen::Matrix<float, 7, 1> Vector7f;
Vector7f t2vFull(const Eigen::Isometry3f& iso);
}
