#include "mpr_pyramid_generator.h"
#include "mpr_utils.h"
#include <srrg_image_utils/depth_utils.h>
#include <srrg_image_utils/point_image_utils.h>

namespace srrg_mpr {
using namespace std;

void MPRPyramidGenerator::WorkspaceEntry::resize(const int rows,
                                                 const int cols) {
  _rows = rows;
  _cols = cols;
  _direction_vectors.create(rows, cols);
  _intensity.create(rows, cols);
  _depth.create(rows, cols);
  _scaled_normals.create(rows, cols);
  _normals.create(rows, cols);
}

MPRPyramidGenerator::MPRPyramidGenerator() { _containers_ready = false; }

void MPRPyramidGenerator::checkImageSizes(
    const srrg_core::RawDepthImage& depth_image,
    const srrg_core::RGBImage& rgb_image) {
  if (depth_image.size() != cv::Size(_config.cols, _config.rows)) {
    cerr << depth_image.rows << "x" << depth_image.cols << endl;
    cerr << _config.rows << "x" << _config.cols << endl;
    throw std::runtime_error("depth image size mismatch");
  }
  if (rgb_image.size() != cv::Size(_config.cols, _config.rows)) {
    throw std::runtime_error("rgb image size mismatch");
  }
}

void MPRPyramidGenerator::setImages(const srrg_core::RawDepthImage& raw_depth,
                                    const srrg_core::RGBImage& raw_rgb) {
  checkImageSizes(raw_depth, raw_rgb);
  _raw_depth = raw_depth;
  _mask = ((_raw_depth == 0));

  // grow the mask
  srrg_core::growMaskImage(_mask, _mask, _config.normals_blur_region_size);

  // convert to mm
  _depth = _raw_depth * 1e-3f;
  // Convert rgb image to grayscale. First convert uint8_t to 1 channel,
  // then convert the 1 channel image to float type.
  cv::cvtColor(raw_rgb, _raw_intensity, CV_RGB2GRAY);
  _intensity = _raw_intensity * (1. / 255);
}

void MPRPyramidGenerator::prepareContainers() {
  if (_containers_ready) { return; }
  cerr << __PRETTY_FUNCTION__ << endl;
  if (_config.camera_matrix == Eigen::Matrix3f::Identity() ||
      _config.rows == 0 || _config.cols == 0) {
    throw std::runtime_error("Cannot prepare containers.");
  }

  size_t num_levels = _config.scales.size();
  _pyramid.resize(num_levels);
  _workspace.resize(num_levels);
  for (size_t level_num = 0; level_num < num_levels; ++level_num) {
    int scale_row = _config.scales[level_num].first;
    int scale_col = _config.scales[level_num].second;
    const float rows = (float)_config.rows / scale_row;
    const float cols = (float)_config.cols / scale_col;

    MPRPyramidLevel& pyr = _pyramid[level_num];
    pyr = MPRPyramidLevel(rows, cols);
    pyr._min_depth = _config.min_depth;
    pyr._max_depth = _config.max_depth;

    Eigen::Matrix3f& scaled_K = pyr._camera_matrix;
    scaled_K = _config.camera_matrix;
    scaled_K.row(0) *= (1. / scale_col);
    scaled_K.row(1) *= (1. / scale_row);

    pyr._sensor_offset = constConfig().sensor_offset;
    pyr._camera_type = constConfig().camera_type;
    WorkspaceEntry& ws = _workspace[level_num];
    ws.resize(rows, cols);
    switch (constConfig().camera_type) {
      case MPRPyramidLevel::Pinhole:
        srrg_core::initializePinholeDirections(ws._direction_vectors, scaled_K);
        break;
      case MPRPyramidLevel::Spherical:
        srrg_core::initializeSphericalDirections(ws._direction_vectors,
                                                 scaled_K);
        break;
      default: throw std::runtime_error("unknown camera model");
    }
  }

  // compute image directions
  _direction_vectors.create(_config.rows, _config.cols);
  srrg_core::initializePinholeDirections(_direction_vectors,
                                         _config.camera_matrix);
  _top_pyramid.resize(_config.rows, _config.cols);
  _containers_ready = true;
}

void MPRPyramidGenerator::computeDerivatives(MPRPyramidLevel& pyr) {
  using namespace srrg_core;

  // these are for the normalization
  const float i2 = pow(_config.intensity_derivative_threshold, 2);
  const float d2 = pow(_config.depth_derivative_threshold, 2);
  const float n2 = pow(_config.depth_derivative_threshold, 2);

  const int rows = pyr._rows;
  const int cols = pyr._cols;

  // we start from 1st row
  for (int r = 1; r < rows - 1; ++r) {
    // fetch the row vectors
    // in the iteration below we start from the 1st column
    // so we increment the pointers by 1

    unsigned char* mask_ptr = pyr._mask.ptr<unsigned char>(r) + 1;
    const MPRPyramidImageEntry* entry_ptr_r0 = pyr._image.rowPtr(r - 1) + 1;
    MPRPyramidImageEntry* entry_ptr = pyr._image.rowPtr(r) + 1;
    const MPRPyramidImageEntry* entry_ptr_r1 = pyr._image.rowPtr(r + 1) + 1;

    for (int c = 1; c < cols - 1;
         ++c, ++mask_ptr, ++entry_ptr_r0, ++entry_ptr, ++entry_ptr_r1) {
      const Vector5f& point_r0 = entry_ptr_r0->_point;
      const Vector5f& point_r1 = entry_ptr_r1->_point;
      const Vector5f& point_c0 = (entry_ptr - 1)->_point;
      const Vector5f& point_c1 = (entry_ptr + 1)->_point;
      Matrix5_2f& derivatives = entry_ptr->_derivatives;

      derivatives.col(1) = .5 * point_r1 - .5 * point_r0;
      derivatives.col(0) = .5 * point_c1 - .5 * point_c0;

      // here we ignore, clamp or suppress
      // the derivatives according to the selected policy
      float inorm2 = derivatives.row(0).squaredNorm();
      if (inorm2 > i2) {
        float nfactor = sqrt(i2 / inorm2);
        switch (_config.intensity_policy) {
          case Suppress: *mask_ptr = 1; continue;
          case Clamp: derivatives.row(0) *= nfactor; break;
          default:;
        }
      }

      float dnorm2 = derivatives.row(1).squaredNorm();
      if (dnorm2 > d2) {
        float nfactor = sqrt(d2 / dnorm2);
        switch (_config.depth_policy) {
          case Suppress: *mask_ptr = 1; continue;
          case Clamp: derivatives.row(1) *= nfactor; break;
          default:;
        }
      }

      float nnorm2 = derivatives.block<3, 2>(2, 0).squaredNorm();
      if (nnorm2 > n2) {
        float nfactor = sqrt(n2 / nnorm2);
        switch (_config.normals_policy) {
          case Suppress: *mask_ptr = 1; continue;
          case Clamp: derivatives.block<3, 2>(2, 0) *= nfactor; break;
          default:;
        }
      }
    }
  }
}

void MPRPyramidGenerator::makeCloud(MPRPyramidLevel& pyr,
                                    const Float3Image& direction_vectors) {
  int rows = pyr._rows;
  int cols = pyr._cols;
  pyr._cloud.resize(rows * cols);
  int num_points = 0;
  for (int r = 0; r < rows; ++r) {
    const unsigned char* mask_ptr =
        pyr._mask.empty() ? 0 : pyr._mask.ptr<const unsigned char>(r);
    const MPRPyramidImageEntry* entry_ptr = pyr._image.rowPtr(r);
    const cv::Vec3f* dir_ptr = direction_vectors.ptr<const cv::Vec3f>(r);
    for (int c = 0; c < cols; ++c, ++entry_ptr, ++dir_ptr) {
      if (!mask_ptr || !*mask_ptr) {
        const MPRPyramidImageEntry& entry = *entry_ptr;
        srrg_core::RichPoint3D& cloud_point = pyr._cloud[num_points];
        Eigen::Map<const Eigen::Vector3f> dir(&((*dir_ptr)[0]));
        const float& intensity = entry._point(0);
        const float& depth = entry._point(1);
        const Eigen::Vector3f& normal = entry._point.block<3, 1>(2, 0);
        cloud_point = srrg_core::RichPoint3D(
            constConfig().sensor_offset * (dir * depth),
            constConfig().sensor_offset.linear() * normal, 1.,
            Eigen::Vector3f(intensity, intensity, intensity));
        ++num_points;
      }

      if (mask_ptr) { ++mask_ptr; }
    }
  }
  if (num_points)
    pyr._cloud.resize(num_points);
  else
    pyr._cloud.clear();
}

void MPRPyramidGenerator::compute() {
  using namespace std;
  using namespace srrg_core;

  if (!_config.rows || !_config.cols) {
    throw std::runtime_error("brother, remember you are gonna die!");
  }
  double start_method = srrg_core::getTime();

  prepareContainers();

  // compute Points_Image, i.e. Float3Image containing for each pixel
  // a 3d vector with the 3d point coordinates
  computePointsImage(_points, _direction_vectors, _depth, _config.min_depth,
                     _config.max_depth);

  computeSimpleNormals(_cross_normals, _points,
                       _config.normals_cross_region_range_col,
                       _config.normals_cross_region_range_row,
                       _config.normals_max_endpoint_distance);

  normalBlur(_normals, _cross_normals, _config.normals_blur_region_size);

  _top_pyramid.initialize(_intensity, _depth, _normals);
  _top_pyramid._mask = _mask;

  // iterate over _level_parameter_vector
  // double start_time = srrg_core::getTime();
  size_t num_levels = _config.scales.size();
  for (size_t level_num = 0; level_num < num_levels; ++level_num) {
    MPRPyramidLevel& pyr = _pyramid[level_num];
    WorkspaceEntry& ws = _workspace[level_num];
    cv::Size level_size(pyr._cols, pyr._rows);
    Cloud3D& current_cloud = pyr._cloud;
    const int scale_row = _config.scales[level_num].first;
    const int scale_col = _config.scales[level_num].second;
    if (scale_row == 1 && scale_col == 1) {
      ws._depth = _depth;
      ws._intensity = _intensity;
      ws._normals = _normals;
      pyr._mask = _mask;

    } else {
      // cv::resize(_intensity,  ws.intensity, level_size);
      shrinkDepth(ws._depth, _depth, scale_row, scale_col, true);
      shrinkIntensity(ws._intensity, _intensity, _depth, scale_row, scale_col);
      // cv::resize(_depth, ws.depth, level_size);

      pyr._mask = (ws._depth == 0.f);

      // srrg_core::growMaskImage(pyr._mask,
      // 			    pyr._mask,
      // 			     _config.normals_blur_region_size);

      int hk_scale = .5 * (scale_col + scale_row);

      // cv::resize(_mask, pyr.mask, level_size);
      cv::resize(_normals, ws._scaled_normals, level_size);
      normalBlur(ws._normals, ws._scaled_normals,
                 _config.normals_scaled_blur_multiplier *
                     (0.5 * (_config.normals_cross_region_range_col +
                             _config.normals_cross_region_range_row) /
                          hk_scale +
                      1));
    }
    pyr.initialize(ws._intensity, ws._depth, ws._normals);
    computeDerivatives(pyr);

    makeCloud(pyr, ws._direction_vectors);
  }

  double end_method = srrg_core::getTime();
}

}  // namespace srrg_mpr
