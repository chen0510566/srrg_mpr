add_library(mpr SHARED
  mpr_utils.cpp
  mpr_stats.cpp
  mpr_pyramid.cpp
  mpr_pyramid_generator.cpp
  mpr_solver.cpp
  mpr_aligner.cpp
  mpr_tracker.cpp
  )

target_link_libraries(mpr
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  )
